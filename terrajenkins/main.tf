module "jenkins_master" {
	source ="./module/aws_instance"
	ec2_ami = var.ec2_amis[0]
	ec2_type = var.ec2_types.master
	ec2_keyname= var.ec2_keyname
	ec2_sg = var.ec2_sg
	ec2_tag = var.ec2_tags.master
}

module "jenkins_node1" {
	source ="./module/aws_instance"
	ec2_ami = var.ec2_amis[1]
	ec2_type = var.ec2_types.node1
	ec2_keyname= var.ec2_keyname
	ec2_sg = var.ec2_sg
	ec2_tag = var.ec2_tags.node1
}

module "ansible_runner_common" {
	source ="./module/ansible_runner"
	depends_on = [module.jenkins_master,module.jenkins_node1]
	ansible_nodes_ip = join(",", [module.jenkins_master.instance_ip,module.jenkins_node1.instance_ip])
	playbook_file = var.jenkins_playbooks.common
	user = var.jenkins_user
	pem_file = var.jenkins_pem
}

module "ansible_runner_master" {
	source ="./module/ansible_runner"
	depends_on = [module.jenkins_master, module.ansible_runner_common]
	ansible_nodes_ip = join(",", [module.jenkins_master.instance_ip] )
	playbook_file = var.jenkins_playbooks.master
	user = var.jenkins_user
	pem_file = var.jenkins_pem
}

