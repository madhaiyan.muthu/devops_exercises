variable "ec2_amis" {
	type = list
	description = "Jenkins AWS AMI ids"
	default = ["ami-01581ffba3821cdf3","ami-01581ffba3821cdf3"]
}

variable "ec2_types" {
	type = map
	default = {
		"master" = "t2.micro"
		"node1" = "t2.micro"
	}
	description = "EC2 types of jenkins nodes"
}

variable "ec2_keyname" {
	default = "devops-master"
	description = "EC2 keynames of jenkins nodes"
}

variable "ec2_sg" {
	default = "sg-0f1fde9f411106846"
	description = "EC2 security groups"
}

variable "ec2_tags" {
	type = map
	default = {
		"master" = "Jenkins master instance"
		"node1" = " Jenkins worker instance"
	}
	description = "EC2 instance tags"
}

variable "jenkins_playbooks" {
	type = map
	default = {
		"common" = "./module/ansible_runner/common.yml"
		"master" = "./module/ansible_runner/master.yml"
	}
}

variable "jenkins_user" {
	default = "ubuntu"
}

variable "jenkins_pem" {
	default = "/etc/ansible/devops-master.pem"
}
