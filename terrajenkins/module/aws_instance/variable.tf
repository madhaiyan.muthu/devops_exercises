variable "ec2_ami" {
	description = "Ec2 instance Ubuntu"
}

variable "ec2_type" {
	description = "EC2 instance types"
}

variable "ec2_keyname" {
	description = "EC2 key-file to connect host"
}

variable "ec2_sg" {
	description = "Security groups of EC2"
}
variable "ec2_tag" {
	description = "Tag names for EC2"
}

