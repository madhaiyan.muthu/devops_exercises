#outputs of instance ips
output "instance_ip" {
	value = aws_instance.jenkins_node.private_ip
}
