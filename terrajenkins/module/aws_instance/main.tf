# Provider AWS
provider "aws" {
	region = "ap-southeast-1"
}

# Resource Ec2 instance
resource "aws_instance" "jenkins_node" {
	ami = var.ec2_ami
	instance_type = var.ec2_type
	key_name = var.ec2_keyname
	vpc_security_group_ids = [var.ec2_sg]
	tags = {
		Name = var.ec2_tag
	}
}

