#Resource Local file to print the job names
resource "null_resource" "ansible_runner" {
	provisioner "local-exec" {
		command = "sleep 60; ansible-playbook ${var.playbook_file} -i ${var.ansible_nodes_ip}, -u ${var.user} --key-file ${var.pem_file}"
	}
}

