variable "playbook_file" {
	description = "Running ansible playbook file"
}

variable "user" {
	description = "Default users to connect on nodes"
}

variable "pem_file" {
	description = "Nodes pem file"
}

variable "ansible_nodes_ip" {
	description = "Ansible node ips"
}
